/**
 * @file BagInterface.h
 * @authors Carrano & Henry, Jim Daehn, <Zhihao Yang>
 * @breif Homework Assignment #2, CSC232 - Missouri State University, Fall 2016
 */

#ifndef BAG_INTERFACE_H
#define BAG_INTERFACE_H

#include <vector>

template<class ItemType>
class BagInterface
{
    /**
     * Gets the current number of entries in this bag.
     * @return The integer number of entries in the bag.
     */
    virtual int getCurrentSize() const = 0;

    /**
     * Sees whether this bag is empty.
     * @return True if the bag is empty, or false if not.
     */
    virtual bool isEmpty() const = 0;

    /**
     * Adds a new entry to this bag.
     * @post If successful, newEntry is stored in the bag and the count of items
     * in the bag has incremented by 1.
     * @param newEntry The object to be added as a new entry.
     * @return True if addition was successful or false if not.
     */
    virtual bool add(const ItemType& newEntry) = 0;

    /**
     * Removes one occurrence of a given entry from this bag if possible.
     * @post If successful, anEntry has been removed from the bag and the count
     * of items in the bag has decreased by 1.
     * @param anEntry The entry to be removed.
     * @return True if removal was successful, or false if not.
     */
    virtual bool remove(const ItemType& anEntry) = 0;

    /**
     * Removes all entries from this bag.
     * @post Bag contains no items, and the count of items is 0.
     */
    virtual void clear() = 0;

    /**
     * Counts the number of times a given entry appears in this bag.
     * @param anEntry The entry to be counted.
     * @return The number of times anEntry appears in the bag.
     */
    virtual int getFrequencyCountOf(const ItemType& anEntry) const = 0;

    /**
     * Tests whether this bag contains a given entry.
     * @param anEntry The entry to locate.
     * @return True if the bag contains anEntry, or false otherwise.
     */
    virtual bool contains(const ItemType& anEntry) const = 0;

    /**
     * Empties and then fills a given vector with all entries that are in this
     * bag.
     * @return A vector containing copies of all the entries in this bag.
     */
    virtual std::vector<ItemType> toVector() const = 0;

    // "CSC232-HW2 - Initial import of Exercise 6 and then push the commit to Bitbucket.
	public BagInterface<ItemType> union(BagInterface<ItemType> anotherBag);
    // CSC232-HW2 - Initial import of Exercise 7 and then push the commit to Bitbucket
	public BagInterface<ItemType> intersection(BagInterface<ItemType> anotherBag);
    // CSC232-HW2 - Initial import of Exercise 8 and then push the commit to Bitbucket.
	public BagInterface<T> difference(BagInterface<T> anotherBag);
    /**
     * Destroys this bag and frees its assigned memory. (See C++ Interlude 2.)
     */
    virtual ~BagInterface() { }
};

#endif /* BAG_INTERFACE_H */
