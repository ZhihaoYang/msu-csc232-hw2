# Homework Assignment #2

## Expanding the BagInterface

## Due: 23:59 Monday 12 September 2016

## Points: 5

## To Do…

1. Fork https://bitbucket.org/professordaehn/msu-csc232-hw2 into a private repo in your Bitbucket account as per our standard operating procedure.
2. Checkout your forked repo and create a develop branch to do your work.
3. Replace <Put Your Name Here> on line 3 of BagInterface.h with your name. Commit this change using the commit message: "CSC232-HW2 - Added name to header comments."
4. Add to BagInterface.h the solution to Exercise 6 from Chapter 1 found on page 28. (See line 72 of BagInterface.h.) When you have completed the exercise, commit your changes with the commit message: "CSC232-HW2 - Initial import of Exercise 6 and then push the commit to Bitbucket.
5. Add to BagInterface.h the solution to Exercise 7 from Chapter 1 found on page 28. (See line 74 of BagInterface.h.) When you have completed the exercise, commit your changes with the commit message: "CSC232-HW2 - Initial import of Exercise 7 and then push the commit to Bitbucket.
6. Add to BagInterface.h the solution to Exercise 8 from Chapter 1 found on page 29. (See line 76 of BagInterface.h.) When you have completed the exercise, commit your changes with the commit message: "CSC232-HW2 - Initial import of Exercise 8 and then push the commit to Bitbucket.
7. Create a pull request to merge your develop branch into the master branch on your private repo. Any pull requests made on my original repo will be declined and you will receive no credit for this assignment.
Log onto Blackboard and navigate to Content > Homework to find the link to submit this assignment for grading. Submit the assignment by entering the URL of your pull request -- as a working hyperlink -- in the Text Submission field of the assignment. It is the timestamp on this last step that dictates whether the assignment as been submitted on time or not. That is, regardless of when the commits were made to your repo, the assignment is considered late if not submitted on Blackboard by the due date

If you have *any* questions, do not hesitate to call, text, video chat, etc. me over the weekend.

## Grading

1. (1 point) Pull Request: Did the student
    1. properly fork my repo into a private repo in their account?
    1. create a develop branch within which to do the work?
    1. create a pull request on their private repo?
    1. submit assignment to Blackboard on time?
1. (1 point) Comment Quality: Did the student
    1. use proper grammar -- including spelling -- in their comments?
    1. write comments that fully specify each operation?
    1. use the appropriate Doxygen tags (e.g., `@param`, `@pre`, `@post`, etc. See Appendix I) to support the documentation?
1. (1 point) Union: Did the student
    1. create the correct method signature for the union operation?
    1. make a commit with an appropriate commit message upon the completion of this specification?
1. (1 point) Intersection:  Did the student
    1. create the correct method signature for the intersection operation?
    1. make a commit with an appropriate commit message upon the completion of this specification?
1. (1 point) Difference:  Did the student
    1. create the correct method signature for the difference operation?
    1. make a commit with an appropriate commit message upon the completion of this specification?